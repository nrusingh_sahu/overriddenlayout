/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SL_LIB122_Test_OverriddenLayout 
{

    static testMethod void myUnitTest() 
    {        
        Account objAccount = new Account(Name='Test Account');
        insert objAccount;
        
        Apexpages.currentPage().getParameters().put('id',objAccount.Id);
        Apexpages.currentPage().getParameters().put('SObject','Account');
        Apexpages.currentPage().getParameters().put('RecordType',[SELECT RecordTypeId FROM Account WHERE Id=:objAccount.Id].RecordTypeId);
        
        SL_LIB122_OverriddenLayoutController objSL_LIB122_OverriddenLayoutControllerforupdate = new SL_LIB122_OverriddenLayoutController();
        
        objSL_LIB122_OverriddenLayoutControllerforupdate.saveSObject();
        objSL_LIB122_OverriddenLayoutControllerforupdate.cancelSObject();
        objSL_LIB122_OverriddenLayoutControllerforupdate.saveandnewSObject();
        
        Apexpages.currentPage().getParameters().put('id',null);
        Apexpages.currentPage().getParameters().put('RecordType',null);
        SL_LIB122_OverriddenLayoutController objSL_LIB122_OverriddenLayoutControllerforcreate = new SL_LIB122_OverriddenLayoutController();
        objSL_LIB122_OverriddenLayoutControllerforcreate.cancelSObject();
    }
}