public with sharing class SL_LIB122_OverriddenLayoutController
{
    /*Start - Variables*/
    public SObject objSObject 					{	get;set;	} //Variable to hold the Sobject
    public String strObjectName 				{	get;set;	} //Variable to hold the name of the Sobject
    public String recordTypeId 					{	get;set;	}//Variable to hold the Id of the currently loggedin user's recordtype
    public Id SObjectId							{	get;set;	}// Id of the current record
    public String strObjectLabel				{	get;set;	}// Label of the current Sobject
    public List<RecordType> lstRecordType		{	get;set;	}// List to get all record types of current Sobject.
    public Boolean isNameExist					{	get;set;	}// Boolean variable to check if 'Name' field exist in List of field APIs.
    public List<FieldWrapper> lstFieldWrapper	{	get;set;	}// List to hold the values 
    private String strQuery;// SOQL to fetch data from Sobject
    private String strKeyPrefix;// KeyPrefix of the current Sobject
    private Map<String, Schema.SObjectType> mapSObjectNameToSObject;//Map of Sobjectname to Sobject.
    /*End - Variables*/
    
    /*Start - Constructor*/
    public SL_LIB122_OverriddenLayoutController() 
    {       
        try 
        {    
        	mapSObjectNameToSObject = Schema.getGlobalDescribe();//Fetching all the objects in the schema.
            SObjectId=Apexpages.currentPage().getParameters().get('id');//Getting an Id of current Sobject
            init();//Calling an init method.
            recordTypeId = Apexpages.currentPage().getParameters().get('RecordType');
            if(recordTypeId==null || recordTypeId=='')
            	getRecordTypes(strObjectName);//Calling function to get RecordType ids.
            if(SObjectId==null)
            {
	            if(objSObject.getSObjectType().getDescribe().fields.getMap().containskey('recordtypeid'))
		                objSObject.put('RecordTypeId',recordTypeId);//Putting the recordtypeid in the RecordTypeId field.
            }
        }
        catch(Exception e) 
        {           
            system.debug('## Exception : '+e+'## Line Number : '+e.getLineNumber());
        }
    }
    /* End - Constructor  */
    
    /*Start - Method to initialize variables*/
    private void init() 
    {       
        strKeyPrefix = recordTypeId = strObjectName = '';
        isNameExist = true;
        lstFieldWrapper = new List<FieldWrapper>();//Instantiating the FieldWrapper class
        lstRecordType = new List<RecordType>();//Instantiating the list.
        if(SObjectId!=null)// For Edit Custom Button,after clicking on edit button on detail page
        {        	
	        strKeyPrefix  = String.valueOf(SObjectId).subString(0,3);	        
	        
	        //Iterating on map of Sobjects to create sobject which has same keyprefix as strKeyPrefix
	        for(Schema.SObjectType objectInstance : mapSObjectNameToSObject.values())
	        {
	            if(objectInstance.getDescribe().getKeyPrefix() == strKeyPrefix)
	            {
	                objSObject = objectInstance.newSObject();//Instantiating objSobject
	                strObjectName = string.valueOf(objSObject);//Getting the name of Sobject
	                strObjectName = strObjectName.subString(0, strObjectName.length() - 3);//Removing the ':{}' from strObjectName
	                strObjectLabel=objectInstance.getDescribe().getLabel();//Getting the label of sobject to display on VF page
	            }
	        }
	        
	        //Checking if the Name field exist or not
	        if(!objSObject.getSObjectType().getDescribe().fields.getMap().containskey('name'))
	               	isNameExist = false;
	               	
	        strQuery = 'SELECT Id ' ;   
	        
	        //Iterating on sobject fields to frame the SOQL.
	        for (Schema.SObjectField f : objSObject.getSObjectType().getDescribe().fields.getMap().values())
	        {
	            lstFieldWrapper.add(new FieldWrapper('Id',false));
	            if(string.valueOf(f).trim().toLowerCase() != 'id')//Validating that sobject field is not Id.
	            {													
	             	strQuery += ','+ string.valueOf(f).trim() + ' ' ;//Framing the SOQL
	             	if(!f.getDescribe().isHtmlFormatted())//Checking if the field type is richtextarea
	             		lstFieldWrapper.add(new FieldWrapper(string.valueOf(f).trim(),false));
	             	else
	             		lstFieldWrapper.add(new FieldWrapper(string.valueOf(f).trim(),true));
	            }														
	        }      
	        strQuery += ' FROM ' + strObjectName+ ' WHERE Id = \''+SObjectId + '\' LIMIT 1';      
	        List<Sobject> lstSobject = Database.query(strQuery);//Executing the SOQL and assign the result to list.
	        if(!lstSobject.isEmpty() && lstSobject!=null)
	           objSObject = lstSobject[0];
        }
        else //For New Custom Button,after clicking on new button(List View Button)
        {
        	//Getting Sobject name.
        	strObjectName=Apexpages.currentPage().getParameters().get('SObject');
        	
        	//Validating SObjectName
	        try
        	{
		        if(!mapSObjectNameToSObject.keyset().Contains(strObjectName.toLowerCase()))
		        	throw new SL_LIB122_MissingArgumentException('Incorrect SObject :' + strObjectName + 'Please supply correct object name.');     
        	}
        	catch(Exception ex)
        	{
        		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR , ex.getMessage()));
        		return;
        	}
        	    		       	
        	for(Schema.SObjectType objectInstance : mapSObjectNameToSObject.values())
	        {
	            if(objectInstance.getDescribe().getName() == strObjectName)
	            {             
	                objSObject = objectInstance.newSObject();//Creating New Sobject.
	                strObjectLabel=objectInstance.getDescribe().getLabel();
	                if(objSObject.getSObjectType().getDescribe().fields.getMap().containskey('ownerid'))
	                	objSObject.put('OwnerId',Userinfo.getUserId());
	            }
	        }
	        
	        for (Schema.SObjectField f : objSObject.getSObjectType().getDescribe().fields.getMap().values()) 
       		{
	            if(!f.getDescribe().isHtmlFormatted())//Checking if the field is of type richtextarea
	            	lstFieldWrapper.add(new FieldWrapper(string.valueOf(f).trim(),false));
	            else
	            	lstFieldWrapper.add(new FieldWrapper(string.valueOf(f).trim(),true));
        	}            
        }   
    }
    /*End - Method to initialize variables*/ 
    
    /*Start - Method to save the record after editing*/
    public PageReference saveSObject()
    {
    	try
    	{
	        upsert objSObject;       
    	}
    	catch(Exception e)
    	{
    		system.debug('## Exception : '+e+' ## Line Number '+e.getLineNumber());
    	}
    	return new Pagereference('/'+objSObject.Id);
    }
    /*End - Method to save the record after editing*/
    
    /*Start - Method to cancel */
    public PageReference cancelSObject()
    {
        if(SObjectId!=null)// In case of edit
        	return new Pagereference('/'+SObjectId);
        else //In case of create new sobject
        	{
        		Pagereference newPage=new Pagereference('/'+objSObject.getSObjectType().getDescribe().getKeyPrefix()+'/o');
        		newPage.setRedirect(true);
        		return newPage;
        	}
    }
    /*End - Method to cancel*/
    
    /*Start - Method to save the existing record after editing and launch the page for new record*/
    public PageReference saveandnewSObject()
    {
        upsert objSObject;
        PageReference newPage = New PageReference('/apex/SL_LIB122_OverriddenLayout?SObject='+strObjectName);
        newPage.setRedirect(true);
        return newPage;
    }
    /*End - Method to save the existing record after editing and launch the page for new record*/
    
	/*Start - Method to fetch Record type Id and name of the current Sobject.*/
	private void getRecordTypes(String strObjectName)
	{
		lstRecordType = [SELECT Id,Name FROM RecordType WHERE SobjectType =: strObjectName];
	}
	/*End - Method to fetch Record type Id and name of the current Sobject.*/
	
	/* Start - Wrapper Class
       Purpose: Using this Wrapper class we can keep track of the fields name and their type.This class is generic enough to 
       hold the field information of any Sobject */
	public class FieldWrapper
	{
		public String strFieldAPIName		{	 get;set;	}
		public Boolean isRichText			{	 get;set;	}
		
		public FieldWrapper(String strFieldAPIName, Boolean isRichText)
		{
			this.strFieldAPIName = strFieldAPIName;
			this.isRichText = isRichText;
		}
	}
	/* End - Inner Class */
}